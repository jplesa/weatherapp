package com.android.weatherapp.currentweather.presenters;

import com.android.weatherapp.common.C;
import com.android.weatherapp.currentweather.CurrentWeatherContract;
import com.android.weatherapp.data.OpenWeatherApi;
import com.android.weatherapp.data.models.CurrentWeather;
import com.android.weatherapp.data.models.MainWeatherInfo;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.lang.reflect.Field;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

import static org.mockito.Mockito.validateMockitoUsage;

/**
 * Created by Jurica Plesa on 24.9.2018..
 */
public class CurrentWeatherPresenterTest {

    @Mock
    private CurrentWeatherContract.View mView;
    @Mock
    private OpenWeatherApi mOpenWeatherApi;

    private CurrentWeatherPresenter mPresenter;
    private CurrentWeather mCurrentWeather;

    @SuppressWarnings("RedundantThrows")
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mPresenter = Mockito.spy(new CurrentWeatherPresenter(mView, mOpenWeatherApi, Schedulers.trampoline(), Schedulers.trampoline()));
        Mockito.when(mView.isActive()).thenReturn(true);

        MainWeatherInfo mainWeatherInfo = new MainWeatherInfo(1.0f, 2.0f, 3.0f, 4.0f, 5.0f);
        mCurrentWeather = new CurrentWeather(1, "City", mainWeatherInfo);
    }

    @Test
    public void onLocationPermissionGranted_placeCode_showPlacePicker() {
        int requestCode = CurrentWeatherPresenter.REQUEST_PLACE_PICK_PERMISSION;

        mPresenter.onLocationPermissionGranted(requestCode);

        Mockito.verify(mView).showPlacePicker();
    }

    @Test
    public void onLocationPermissionGranted_locationCode_requestLastLocation() {
        int requestCode = CurrentWeatherPresenter.REQUEST_LAST_LOCATION_PERMISSIONS;

        mPresenter.onLocationPermissionGranted(requestCode);

        Mockito.verify(mView).requestLastLocation();
    }

    @Test
    public void onLocationPermissionGranted_randomCode_doNothing() {
        int requestCode = 0;

        mPresenter.onLocationPermissionGranted(requestCode);

        Mockito.verify(mView, Mockito.never()).showPlacePicker();
        Mockito.verify(mView, Mockito.never()).requestLastLocation();
    }

    @Test
    public void onSearchCityClicked_permissionGranted_showPlacePicker() {
        Mockito.when(mView.isLocationPermissionGranted()).thenReturn(true);

        mPresenter.onSearchCityClicked();

        Mockito.verify(mView).showPlacePicker();
    }

    @Test
    public void onSearchCityClicked_permissionNotGranted_requestLocationPermission() {
        Mockito.when(mView.isLocationPermissionGranted()).thenReturn(false);

        mPresenter.onSearchCityClicked();

        Mockito.verify(mView).requestLocationPermission(CurrentWeatherPresenter.REQUEST_PLACE_PICK_PERMISSION);
    }

    @Test
    public void getCurrentWeatherLatLong_valid_setDataIntoView() {
        double latitude = 10;
        double longitude = 10;

        Mockito.when(mOpenWeatherApi.getCurrentWeather(latitude, longitude)).thenReturn(Observable.just(mCurrentWeather));

        mPresenter.getCurrentWeather(latitude, longitude);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).setCity(mCurrentWeather.getCity());
        Mockito.verify(mView).setTemperature(mCurrentWeather.getMainWeatherInfo().getTemperature());
        Mockito.verify(mView).setHumidity(mCurrentWeather.getMainWeatherInfo().getHumidity());
        Mockito.verify(mView).setPressure(mCurrentWeather.getMainWeatherInfo().getPressure());
        Mockito.verify(mView).setMinTemperature(mCurrentWeather.getMainWeatherInfo().getMinTemperature());
        Mockito.verify(mView).setMaxTemperature(mCurrentWeather.getMainWeatherInfo().getMaxTemperature());
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherLatLong_tooManyRequests_showTooManyRequestsError() {
        double latitude = 10;
        double longitude = 10;

        HttpException httpException = new HttpException(Response.error(C.HTTP_TOO_MANY_REQUESTS, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getCurrentWeather(latitude, longitude)).thenReturn(Observable.<CurrentWeather>error(httpException));

        mPresenter.getCurrentWeather(latitude, longitude);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showMaximumRequestsError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherLatLong_httpException_showServerError() {
        double latitude = 10;
        double longitude = 10;
        int serverCode = 400;

        HttpException httpException = new HttpException(Response.error(serverCode, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getCurrentWeather(latitude, longitude)).thenReturn(Observable.<CurrentWeather>error(httpException));

        mPresenter.getCurrentWeather(latitude, longitude);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherLatLong_throwable_showServerError() {
        double latitude = 10;
        double longitude = 10;

        Mockito.when(mOpenWeatherApi.getCurrentWeather(latitude, longitude)).thenReturn(Observable.<CurrentWeather>error(new Throwable()));

        mPresenter.getCurrentWeather(latitude, longitude);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherCityId_valid_setDataIntoView() {
        int cityId = 1000;

        Mockito.when(mOpenWeatherApi.getCurrentWeather(cityId)).thenReturn(Observable.just(mCurrentWeather));

        mPresenter.getCurrentWeather(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).setCity(mCurrentWeather.getCity());
        Mockito.verify(mView).setTemperature(mCurrentWeather.getMainWeatherInfo().getTemperature());
        Mockito.verify(mView).setHumidity(mCurrentWeather.getMainWeatherInfo().getHumidity());
        Mockito.verify(mView).setPressure(mCurrentWeather.getMainWeatherInfo().getPressure());
        Mockito.verify(mView).setMinTemperature(mCurrentWeather.getMainWeatherInfo().getMinTemperature());
        Mockito.verify(mView).setMaxTemperature(mCurrentWeather.getMainWeatherInfo().getMaxTemperature());
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherCityId_tooManyRequests_showTooManyRequestsError() {
        int cityId = 1000;

        HttpException httpException = new HttpException(Response.error(C.HTTP_TOO_MANY_REQUESTS, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getCurrentWeather(cityId)).thenReturn(Observable.<CurrentWeather>error(httpException));

        mPresenter.getCurrentWeather(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showMaximumRequestsError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherCityId_httpException_showServerError() {
        int cityId = 1000;
        int serverCode = 400;

        HttpException httpException = new HttpException(Response.error(serverCode, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getCurrentWeather(cityId)).thenReturn(Observable.<CurrentWeather>error(httpException));

        mPresenter.getCurrentWeather(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getCurrentWeatherCityId_throwable_showServerError() {
        int cityId = 1000;

        Mockito.when(mOpenWeatherApi.getCurrentWeather(cityId)).thenReturn(Observable.<CurrentWeather>error(new Throwable()));

        mPresenter.getCurrentWeather(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void onForecastButtonClicked_cityIdNotNull() {
        try {
            Integer cityId = 1000;
            Field cityIdField = mPresenter.getClass().getDeclaredField("mLastReceivedCityId");
            cityIdField.setAccessible(true);
            cityIdField.set(mPresenter, cityId);

            mPresenter.onForecastButtonClicked();

            Mockito.verify(mView).startForecastFragment(cityId);

            cityIdField.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void subscribe_cityIdNotNull_getCurrentWeather() {
        try {
            Integer cityId = 1000;
            Field cityIdField = mPresenter.getClass().getDeclaredField("mLastReceivedCityId");
            cityIdField.setAccessible(true);
            cityIdField.set(mPresenter, cityId);

            Mockito.when(mOpenWeatherApi.getCurrentWeather(cityId)).thenReturn(Observable.just(mCurrentWeather));

            mPresenter.subscribe();

            Mockito.verify(mView, Mockito.never()).requestLastLocation();
            Mockito.verify(mView, Mockito.never()).requestLocationPermission(CurrentWeatherPresenter.REQUEST_LAST_LOCATION_PERMISSIONS);

            cityIdField.setAccessible(false);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void subscribe_cityIdNullPermissionGranted_showPlacePicker() {
        Mockito.when(mView.isLocationPermissionGranted()).thenReturn(true);

        mPresenter.subscribe();

        Mockito.verify(mView).requestLastLocation();
    }

    @Test
    public void subscribe_cityIdNullPermissionNotGranted_requestLocationPermission() {
        Mockito.when(mView.isLocationPermissionGranted()).thenReturn(false);

        mPresenter.subscribe();

        Mockito.verify(mView).requestLocationPermission(CurrentWeatherPresenter.REQUEST_LAST_LOCATION_PERMISSIONS);
    }

    @After
    public void validate() {
        validateMockitoUsage();
    }

}