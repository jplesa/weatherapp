package com.android.weatherapp.forecast.presenters;

import com.android.weatherapp.common.C;
import com.android.weatherapp.data.OpenWeatherApi;
import com.android.weatherapp.data.models.City;
import com.android.weatherapp.data.models.WeatherForecast;
import com.android.weatherapp.forecast.ForecastContract;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;

import static org.mockito.Mockito.validateMockitoUsage;

/**
 * Created by Jurica Plesa on 26.9.2018..
 */
public class ForecastPresenterTest {

    @Mock
    private ForecastContract.View mView;
    @Mock
    private OpenWeatherApi mOpenWeatherApi;

    private ForecastPresenter mPresenter;

    @SuppressWarnings("RedundantThrows")
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mPresenter = Mockito.spy(new ForecastPresenter(mView, mOpenWeatherApi, Schedulers.trampoline(), Schedulers.trampoline()));
        Mockito.when(mView.isActive()).thenReturn(true);

    }

    @Test
    public void getForecast_validRequest_setDataIntoView() {
        int cityId = 1000;

        WeatherForecast weatherForecast = Mockito.mock(WeatherForecast.class);
        City city = Mockito.mock(City.class);

        Mockito.when(weatherForecast.getCity()).thenReturn(city);
        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.just(weatherForecast));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).setCity(weatherForecast.getCity().getName());
        Mockito.verify(mView).onForecastReceived(weatherForecast.getForecast());
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getForecast_responseCityNull_setOnlyForecastIntoView() {
        int cityId = 1000;

        WeatherForecast weatherForecast = Mockito.mock(WeatherForecast.class);
        weatherForecast.setCity(null);

        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.just(weatherForecast));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView, Mockito.never()).setCity(null);
        Mockito.verify(mView).onForecastReceived(weatherForecast.getForecast());
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getForecast_responseForecastNull_setOnlyCityIntoView() {
        int cityId = 1000;

        WeatherForecast weatherForecast = Mockito.mock(WeatherForecast.class);
        City city = Mockito.mock(City.class);

        Mockito.when(weatherForecast.getCity()).thenReturn(city);
        Mockito.when(weatherForecast.getForecast()).thenReturn(null);
        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.just(weatherForecast));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).setCity(weatherForecast.getCity().getName());
        Mockito.verify(mView, Mockito.never()).onForecastReceived(weatherForecast.getForecast());
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getForecast_tooManyRequests_showTooManyRequestsError() {
        int cityId = 1000;

        HttpException httpException = new HttpException(Response.error(C.HTTP_TOO_MANY_REQUESTS, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.<WeatherForecast>error(httpException));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showMaximumRequestsError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getForecast_httpException_showServerError() {
        int cityId = 1000;
        int serverCode = 400;

        HttpException httpException = new HttpException(Response.error(serverCode, ResponseBody.create(MediaType.parse("type"), "")));
        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.<WeatherForecast>error(httpException));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @Test
    public void getForecast_throwable_showServerError() {
        int cityId = 1000;

        Mockito.when(mOpenWeatherApi.getWeatherForecast(cityId)).thenReturn(Observable.<WeatherForecast>error(new Throwable()));

        mPresenter.getForecast(cityId);

        Mockito.verify(mView).showLoadingIndicator();
        Mockito.verify(mView).showServerError();
        Mockito.verify(mView).hideLoadingIndicator();
    }

    @After
    public void validate() {
        validateMockitoUsage();
    }

}