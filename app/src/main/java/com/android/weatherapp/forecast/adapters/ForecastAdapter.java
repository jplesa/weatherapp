package com.android.weatherapp.forecast.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.weatherapp.R;
import com.android.weatherapp.common.C;
import com.android.weatherapp.common.utils.FormatDateUtils;
import com.android.weatherapp.data.models.Forecast;
import com.android.weatherapp.data.models.MainWeatherInfo;
import com.android.weatherapp.data.models.WeatherDescription;

import java.util.ArrayList;

/**
 * Created by Jurica Plesa on 25.9.2018..
 */
public class ForecastAdapter extends RecyclerView.Adapter<ForecastAdapter.ForecastViewHolder> {

    private ArrayList<Forecast> mData = new ArrayList<>();

    public void setData(@NonNull ArrayList<Forecast> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ForecastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_forecast, parent, false);
        return new ForecastViewHolder(view);
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onBindViewHolder(@NonNull ForecastViewHolder holder, int position) {
        Forecast forecast = mData.get(position);
        if (forecast != null) {
            if (!TextUtils.isEmpty(forecast.getDate())) {
                holder.mDate.setText(FormatDateUtils.formatUTCdateFromString(
                        C.OPEN_WEATHER_API_DATE_PATTERN,
                        C.FORECAST_VIEW_DATE_PATTERN,
                        forecast.getDate()));
            }
            MainWeatherInfo mainWeatherInfo = forecast.getMainWeatherInfo();
            if (mainWeatherInfo != null) {
                String temperatureWithSymbol = mainWeatherInfo.getTemperature() + " " + holder.itemView.getResources().getString(R.string.celsius);
                holder.mTemperature.setText(temperatureWithSymbol);
            }
            ArrayList<WeatherDescription> weatherDescription = forecast.getWeatherDescription();
            if (weatherDescription != null && !weatherDescription.isEmpty()) {
                holder.mWeatherDescription.setText((String.valueOf(weatherDescription.get(0).getDescription())));
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    class ForecastViewHolder extends RecyclerView.ViewHolder {

        TextView mDate;
        TextView mTemperature;
        TextView mWeatherDescription;

        ForecastViewHolder(View itemView) {
            super(itemView);

            mDate = itemView.findViewById(R.id.txt_date);
            mTemperature = itemView.findViewById(R.id.txt_temperature);
            mWeatherDescription = itemView.findViewById(R.id.txt_weather_description);
        }
    }

}
