package com.android.weatherapp.forecast;

import com.android.weatherapp.common.base.BaseContractPresenter;
import com.android.weatherapp.common.base.BaseContractView;
import com.android.weatherapp.data.models.Forecast;

import java.util.ArrayList;

/**
 * Created by Jurica Plesa on 24.9.2018..
 */
@SuppressWarnings("unused")
public interface ForecastContract {

    interface View extends BaseContractView {

        void onForecastReceived(ArrayList<Forecast> forecasts);

        void setCity(String city);

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showServerError();

        void showMaximumRequestsError();

    }

    interface Presenter extends BaseContractPresenter {

        void getForecast(int cityId);

    }
}
