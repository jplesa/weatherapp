package com.android.weatherapp.forecast.presenters;

import com.android.weatherapp.common.C;
import com.android.weatherapp.data.OpenWeatherApi;
import com.android.weatherapp.data.models.WeatherForecast;
import com.android.weatherapp.forecast.ForecastContract;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Created by Jurica Plesa on 24.9.2018..
 */
public class ForecastPresenter implements ForecastContract.Presenter {

    private ForecastContract.View mView;
    private OpenWeatherApi mOpenWeatherApi;
    private Scheduler mProcessScheduler;
    private Scheduler mAndroidScheduler;
    private CompositeDisposable mDisposables = new CompositeDisposable();

    public ForecastPresenter(ForecastContract.View view, OpenWeatherApi openWeatherApi, Scheduler processScheduler, Scheduler androidScheduler) {
        this.mView = view;
        this.mOpenWeatherApi = openWeatherApi;
        this.mProcessScheduler = processScheduler;
        this.mAndroidScheduler = androidScheduler;
    }

    @Override
    public void getForecast(int cityId) {
        if (!mView.isActive()) {
            return;
        }

        DisposableObserver<WeatherForecast> weatherForecastObserver = getWeatherForecastObserver();
        mDisposables.add(weatherForecastObserver);

        mView.showLoadingIndicator();

        mOpenWeatherApi.getWeatherForecast(cityId)
                .subscribeOn(mProcessScheduler)
                .observeOn(mAndroidScheduler, true)
                .subscribe(weatherForecastObserver);
    }

    @Override
    public void subscribe() {
        //no action
    }

    @Override
    public void unsubscribe() {
        mDisposables.clear();
    }

    private DisposableObserver<WeatherForecast> getWeatherForecastObserver() {
        return new DisposableObserver<WeatherForecast>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onNext(WeatherForecast weatherForecast) {
                if (!mView.isActive()) {
                    return;
                }

                if (weatherForecast != null) {
                    if (weatherForecast.getCity() != null) {
                        mView.setCity(weatherForecast.getCity().getName());
                    }
                    if (weatherForecast.getForecast() != null) {
                        mView.onForecastReceived(weatherForecast.getForecast());
                    }
                }
                mView.hideLoadingIndicator();
            }

            @Override
            public void onError(Throwable e) {
                if (!mView.isActive()) {
                    return;
                }
                if (e instanceof HttpException) {
                    int code = ((HttpException) e).code();
                    switch (code) {
                        case C.HTTP_TOO_MANY_REQUESTS:
                            mView.showMaximumRequestsError();
                            break;
                        default:
                            mView.showServerError();
                            break;
                    }
                } else {
                    mView.showServerError();
                }
                mView.hideLoadingIndicator();
            }

            @Override
            public void onComplete() {

            }
        };
    }

}
