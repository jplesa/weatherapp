package com.android.weatherapp.forecast.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.weatherapp.R;
import com.android.weatherapp.common.Extra;
import com.android.weatherapp.common.base.BaseFragment;
import com.android.weatherapp.data.DataProvider;
import com.android.weatherapp.data.models.Forecast;
import com.android.weatherapp.forecast.ForecastContract;
import com.android.weatherapp.forecast.adapters.ForecastAdapter;
import com.android.weatherapp.forecast.presenters.ForecastPresenter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Jurica Plesa on 24.9.2018..
 */
public class ForecastFragment extends BaseFragment implements ForecastContract.View {

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private Unbinder mButterKnifeUnbinder;
    private ForecastPresenter mPresenter;
    private ForecastAdapter mAdapter;

    public static ForecastFragment newInstance(int cityId) {
        Bundle args = new Bundle();
        args.putInt(Extra.EXTRA_CITY_ID, cityId);
        ForecastFragment fragment = new ForecastFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mPresenter = new ForecastPresenter(this, DataProvider.getInstance().getOpenWeatherApi(), Schedulers.io(), AndroidSchedulers.mainThread());
        mAdapter = new ForecastAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        mButterKnifeUnbinder = ButterKnife.bind(this, view);
        setupToolbar();
        setupListViews();

        Bundle bundle = this.getArguments();
        if (bundle != null && bundle.containsKey(Extra.EXTRA_CITY_ID)) {
            mPresenter.getForecast(bundle.getInt(Extra.EXTRA_CITY_ID));
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (getActivity() != null) {
                    getActivity().onBackPressed();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onForecastReceived(ArrayList<Forecast> forecasts) {
        mAdapter.setData(forecasts);
    }

    @Override
    public void setCity(String city) {
        mToolbarTitle.setText(city);
    }

    @Override
    public void showLoadingIndicator() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showServerError() {
        Toast.makeText(getContext(), R.string.common_server_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMaximumRequestsError() {
        Toast.makeText(getContext(), R.string.common_maximum_requests_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mButterKnifeUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.unsubscribe();
        }
        super.onDestroy();
    }

    private void setupToolbar() {
        setActionBar(mToolbar);
        setHomeAsUp();
    }

    private void setupListViews() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }
}
