package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class MainWeatherInfo {

    @SerializedName("temp")
    private Float temperature;
    @SerializedName("pressure")
    private Float pressure;
    @SerializedName("humidity")
    private Float humidity;
    @SerializedName("temp_min")
    private Float minTemperature;
    @SerializedName("temp_max")
    private Float maxTemperature;

    public MainWeatherInfo() {
    }

    public MainWeatherInfo(Float temperature, Float pressure, Float humidity, Float minTemperature, Float maxTemperature) {
        this.temperature = temperature;
        this.pressure = pressure;
        this.humidity = humidity;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public Float getPressure() {
        return pressure;
    }

    public void setPressure(Float pressure) {
        this.pressure = pressure;
    }

    public Float getHumidity() {
        return humidity;
    }

    public void setHumidity(Float humidity) {
        this.humidity = humidity;
    }

    public Float getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(Float minTemperature) {
        this.minTemperature = minTemperature;
    }

    public Float getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(Float maxTemperature) {
        this.maxTemperature = maxTemperature;
    }
}
