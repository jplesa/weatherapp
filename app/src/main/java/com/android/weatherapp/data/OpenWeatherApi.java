package com.android.weatherapp.data;

import com.android.weatherapp.data.models.CurrentWeather;
import com.android.weatherapp.data.models.WeatherForecast;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jurica Plesa on 22.9.2018..
 */
public class OpenWeatherApi {

    private OpenWeatherService mDataService;

    private OpenWeatherApi(Builder builder) {
        OkHttpClient client = (builder.mClient != null) ? builder.mClient : new OkHttpClient();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(builder.mBaseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        mDataService = retrofit.create(OpenWeatherService.class);
    }

    public static class Builder {
        private final String mBaseUrl;
        private OkHttpClient mClient;

        public Builder(String baseUrl) {
            mBaseUrl = baseUrl;
        }

        @SuppressWarnings("unused")
        public Builder withClient(OkHttpClient client) {
            mClient = client;
            return this;
        }

        public OpenWeatherApi build() {
            return new OpenWeatherApi(this);
        }
    }

    public Observable<CurrentWeather> getCurrentWeather(double latitude, double longitude) {
        return mDataService.getCurrentWeather(latitude, longitude);
    }

    public Observable<CurrentWeather> getCurrentWeather(int cityId) {
        return mDataService.getCurrentWeather(cityId);
    }

    public Observable<WeatherForecast> getWeatherForecast(int cityId) {
        return mDataService.getWeatherForecast(cityId);
    }

}
