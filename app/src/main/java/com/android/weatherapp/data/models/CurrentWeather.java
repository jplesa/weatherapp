package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class CurrentWeather {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String city;
    @SerializedName("main")
    private MainWeatherInfo mainWeatherInfo;

    public CurrentWeather() {
    }

    public CurrentWeather(Integer id, String city, MainWeatherInfo mainWeatherInfo) {
        this.id = id;
        this.city = city;
        this.mainWeatherInfo = mainWeatherInfo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public MainWeatherInfo getMainWeatherInfo() {
        return mainWeatherInfo;
    }

    public void setMainWeatherInfo(MainWeatherInfo mainWeatherInfo) {
        this.mainWeatherInfo = mainWeatherInfo;
    }
}
