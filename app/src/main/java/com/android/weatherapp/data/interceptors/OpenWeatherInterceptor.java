package com.android.weatherapp.data.interceptors;

import android.support.annotation.NonNull;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class OpenWeatherInterceptor implements Interceptor {

    @SuppressWarnings("WeakerAccess")
    public static final String SKIP_KEY = "skip-open-weather-api-key-query";

    private String mApiKey;

    public OpenWeatherInterceptor(String apiKey) {
        this.mApiKey = apiKey;
    }

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request originalRequest = chain.request();

        //Skip if contain skip key
        String headerValue = originalRequest.header(SKIP_KEY);
        if (headerValue != null) {
            return chain.proceed(originalRequest);
        }

        HttpUrl originalHttpUrl = originalRequest.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("APPID", mApiKey)
                .addQueryParameter("units", "metric")
                .build();

        Request.Builder requestBuilder = originalRequest.newBuilder()
                .url(url);

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }

}
