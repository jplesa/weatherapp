package com.android.weatherapp.data;

import com.android.weatherapp.data.models.CurrentWeather;
import com.android.weatherapp.data.models.WeatherForecast;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Jurica Plesa on 22.9.2018..
 */
public interface OpenWeatherService {

    @GET("data/2.5/weather")
    Observable<CurrentWeather> getCurrentWeather(
            @Query("lat") double latitude,
            @Query("lon") double longitude);

    @GET("data/2.5/weather")
    Observable<CurrentWeather> getCurrentWeather(
            @Query("id") int cityId);

    @GET("data/2.5/forecast")
    Observable<WeatherForecast> getWeatherForecast(
            @Query("id") int cityId);

}
