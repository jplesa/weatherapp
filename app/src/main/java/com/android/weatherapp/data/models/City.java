package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class City {

    @SerializedName("id")
    private Integer id;
    @SerializedName("name")
    private String name;

    public City() {
    }

    public City(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
