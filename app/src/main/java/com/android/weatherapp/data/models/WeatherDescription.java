package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class WeatherDescription {

    @SerializedName("description")
    private String description;

    public WeatherDescription() {
    }

    public WeatherDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
