package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class WeatherForecast {

    @SerializedName("city")
    private City city;
    @SerializedName("list")
    private ArrayList<Forecast> forecast;

    public WeatherForecast() {
    }

    public WeatherForecast(City city, ArrayList<Forecast> forecast) {
        this.city = city;
        this.forecast = forecast;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public ArrayList<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(ArrayList<Forecast> forecast) {
        this.forecast = forecast;
    }
}
