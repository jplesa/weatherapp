package com.android.weatherapp.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Jurica Plesa on 27.9.2018..
 */
@SuppressWarnings("unused")
public class Forecast {

    @SerializedName("main")
    private MainWeatherInfo mainWeatherInfo;
    @SerializedName("weather")
    private ArrayList<WeatherDescription> weatherDescription;
    @SerializedName("dt_txt")
    private String date;

    public Forecast() {
    }

    public Forecast(MainWeatherInfo mainWeatherInfo, ArrayList<WeatherDescription> weatherDescription, String date) {
        this.mainWeatherInfo = mainWeatherInfo;
        this.weatherDescription = weatherDescription;
        this.date = date;
    }

    public MainWeatherInfo getMainWeatherInfo() {
        return mainWeatherInfo;
    }

    public void setMainWeatherInfo(MainWeatherInfo mainWeatherInfo) {
        this.mainWeatherInfo = mainWeatherInfo;
    }

    public ArrayList<WeatherDescription> getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(ArrayList<WeatherDescription> weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
