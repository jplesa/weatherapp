package com.android.weatherapp.data;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class DataProvider {

    private static volatile DataProvider sInstance;

    private OpenWeatherApi mOpenWeatherApi;

    private DataProvider() {
        if (sInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static DataProvider getInstance() {
        if (sInstance == null) {
            synchronized (DataProvider.class) {
                if (sInstance == null) {
                    sInstance = new DataProvider();
                }
            }
        }

        return sInstance;
    }

    public void registerOpenWeatherApi(OpenWeatherApi openWeatherApi) {
        this.mOpenWeatherApi = openWeatherApi;
    }

    public OpenWeatherApi getOpenWeatherApi() {
        if(mOpenWeatherApi == null) {
            return new OpenWeatherApi.Builder("http://localhost/").build();
        }

        return mOpenWeatherApi;
    }

}
