package com.android.weatherapp;

import android.app.Application;

import com.android.weatherapp.data.DataProvider;
import com.android.weatherapp.data.OpenWeatherApi;
import com.android.weatherapp.data.interceptors.OpenWeatherInterceptor;

import okhttp3.OkHttpClient;

/**
 * Created by Jurica Plesa on 22.9.2018..
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        registerDataProviders();
    }

    private void registerDataProviders() {
        DataProvider dataProvider = DataProvider.getInstance();

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new OpenWeatherInterceptor(getString(R.string.open_weather_api_key)))
                .build();
        OpenWeatherApi openWeatherApi = new OpenWeatherApi.Builder(getString(R.string.api_base_url))
                .withClient(client)
                .build();
        dataProvider.registerOpenWeatherApi(openWeatherApi);
    }
}
