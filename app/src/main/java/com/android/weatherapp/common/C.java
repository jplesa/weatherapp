package com.android.weatherapp.common;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class C {

    public static final int HTTP_TOO_MANY_REQUESTS = 429;

    public static final String OPEN_WEATHER_API_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String FORECAST_VIEW_DATE_PATTERN = "EEE, HH'h'";

}
