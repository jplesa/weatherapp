package com.android.weatherapp.common.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jurica Plesa on 26.9.2018..
 */
public class FormatDateUtils {

    /**
     * @param inputFormat  pattern of input date as String
     * @param outputFormat pattern of output date as String
     * @param inputDate    date as String to be formatted
     * @return formatted date as String. If not successful, returns input
     */
    public static String formatUTCdateFromString(String inputFormat, String outputFormat, String inputDate) {
        Date date;

        SimpleDateFormat inputDateFormat = new SimpleDateFormat(inputFormat, Locale.getDefault());
        SimpleDateFormat outputDateFormat = new SimpleDateFormat(outputFormat, Locale.getDefault());
        try {
            date = inputDateFormat.parse(inputDate);
        } catch (ParseException e) {
            return inputDate;
        }

        if (date == null) {
            return inputDate;
        }
        return outputDateFormat.format(date);
    }

}
