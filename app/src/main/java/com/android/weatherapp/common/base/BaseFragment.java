package com.android.weatherapp.common.base;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.weatherapp.R;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class BaseFragment extends Fragment {

    /**
     * Adds Toolbar as support action bar
     */
    protected void setActionBar(Toolbar toolbar) {
        if (toolbar == null) {
            return;
        }
        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            activity.setSupportActionBar(toolbar);
            ActionBar supportAB = activity.getSupportActionBar();
            if (supportAB != null) {
                supportAB.setDisplayShowTitleEnabled(false);
            }
        }
    }

    /**
     * Adds Home(back) button on support action bar
     */
    protected void setHomeAsUp() {
        if (getActivity() instanceof AppCompatActivity) {
            AppCompatActivity activity = (AppCompatActivity) getActivity();
            ActionBar supportAB = activity.getSupportActionBar();
            if (supportAB != null) {
                supportAB.setDisplayHomeAsUpEnabled(true);
                supportAB.setHomeButtonEnabled(true);
                supportAB.setHomeAsUpIndicator(R.drawable.ic_back);
            }
        }
    }
}
