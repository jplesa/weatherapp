package com.android.weatherapp.common.base;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public interface BaseContractView {
    boolean isActive();
}
