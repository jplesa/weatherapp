package com.android.weatherapp.common.base;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
@SuppressWarnings("unused")
public interface BaseContractPresenter {
    void subscribe();

    void unsubscribe();
}
