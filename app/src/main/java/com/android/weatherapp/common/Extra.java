package com.android.weatherapp.common;

/**
 * Created by Jurica Plesa on 24.9.2018..
 */
public class Extra {

    public static final String EXTRA_CITY_ID = "weatherapp.extra.city_id";

}
