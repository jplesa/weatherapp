package com.android.weatherapp.currentweather.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.weatherapp.R;
import com.android.weatherapp.common.base.BaseFragment;
import com.android.weatherapp.currentweather.CurrentWeatherContract;
import com.android.weatherapp.currentweather.presenters.CurrentWeatherPresenter;
import com.android.weatherapp.data.DataProvider;
import com.android.weatherapp.forecast.fragments.ForecastFragment;
import com.android.weatherapp.main.MainActivity;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class CurrentWeatherFragment extends BaseFragment implements CurrentWeatherContract.View {

    private static final int PLACE_PICKER_REQUEST = 100;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_title)
    TextView mToolbarTitle;
    @BindView(R.id.txt_city_name)
    TextView mCityName;
    @BindView(R.id.txt_min_temperature)
    TextView mMinTemperature;
    @BindView(R.id.txt_max_temperature)
    TextView mMaxTemperature;
    @BindView(R.id.txt_temperature)
    TextView mTemperature;
    @BindView(R.id.txt_pressure)
    TextView mPressure;
    @BindView(R.id.txt_humidity)
    TextView mHumidity;
    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    private Unbinder mButterKnifeUnbinder;
    private CurrentWeatherPresenter mPresenter;

    public static CurrentWeatherFragment newInstance() {
        return new CurrentWeatherFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mPresenter = new CurrentWeatherPresenter(this, DataProvider.getInstance().getOpenWeatherApi(), Schedulers.io(), AndroidSchedulers.mainThread());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_current_weather, container, false);
        mButterKnifeUnbinder = ButterKnife.bind(this, view);
        setupToolbar();
        mPresenter.subscribe();
        return view;
    }

    @OnClick({R.id.txt_refresh, R.id.btn_forecast})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_refresh:
                mPresenter.onRefreshButtonClicked();
                break;
            case R.id.btn_forecast:
                mPresenter.onForecastButtonClicked();
                break;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_current_weather, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                mPresenter.onSearchCityClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void requestLocationPermission(final int requestCode) {
        if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION) && getContext() != null) {
            new AlertDialog.Builder(getContext())
                    .setTitle(getString(R.string.permission_dialog_title))
                    .setMessage(getString(R.string.permission_dialog_message))
                    .setPositiveButton(getString(R.string.permission_dialog_positive_btn), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
                        }
                    })
                    .show();
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, requestCode);
        }
    }

    @Override
    public boolean isLocationPermissionGranted() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            boolean permissionGranted = false;
            if (getContext() != null) {
                permissionGranted = PermissionChecker.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
            }
            return permissionGranted;
        } else {
            return true;
        }
    }

    @Override
    public void setCity(String city) {
        mCityName.setText(city);
    }

    @Override
    public void setTemperature(Float temperature) {
        if (temperature != null) {
            String temperatureWithSymbol = temperature.toString() + " " + getString(R.string.celsius);
            mTemperature.setText(temperatureWithSymbol);
        }
    }

    @Override
    public void setPressure(Float pressure) {
        if (pressure != null) {
            String pressureWithSymbol = pressure.toString() + " " + getString(R.string.hectopascal);
            mPressure.setText(pressureWithSymbol);
        }
    }

    @Override
    public void setHumidity(Float humidity) {
        if (humidity != null) {
            String humidityWithSymbol = humidity.toString() + getString(R.string.percent);
            mHumidity.setText(humidityWithSymbol);
        }
    }

    @Override
    public void setMinTemperature(Float minTemperature) {
        if (minTemperature != null) {
            String temperatureWithSymbol = minTemperature.toString() + " " + getString(R.string.celsius);
            mMinTemperature.setText(temperatureWithSymbol);
        }
    }

    @Override
    public void setMaxTemperature(Float maxTemperature) {
        if (maxTemperature != null) {
            String temperatureWithSymbol = maxTemperature.toString() + " " + getString(R.string.celsius);
            mMaxTemperature.setText(temperatureWithSymbol);
        }
    }

    @Override
    public void showLoadingIndicator() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingIndicator() {
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showServerError() {
        Toast.makeText(getContext(), R.string.common_server_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMaximumRequestsError() {
        Toast.makeText(getContext(), R.string.common_maximum_requests_error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean isActive() {
        return isAdded();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mButterKnifeUnbinder.unbind();
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.unsubscribe();
        }
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK && getContext() != null) {
                Place place = PlacePicker.getPlace(getContext(), data);
                LatLng placeLatLng = place.getLatLng();
                mPresenter.getCurrentWeather(placeLatLng.latitude, placeLatLng.longitude);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            mPresenter.onLocationPermissionGranted(requestCode);
        }
    }

    @SuppressLint("MissingPermission") //Already checking for permission before call
    @Override
    public void requestLastLocation() {
        if (getActivity() != null) {
            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (location != null) {
                                mPresenter.getCurrentWeather(location.getLatitude(), location.getLongitude());
                            } else {
                                mPresenter.getCurrentWeather(
                                        Double.parseDouble(getString(R.string.default_location_latitude)),
                                        Double.parseDouble(getString(R.string.default_location_longitude)));
                            }
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mPresenter.getCurrentWeather(
                                    Double.parseDouble(getString(R.string.default_location_latitude)),
                                    Double.parseDouble(getString(R.string.default_location_longitude)));
                        }
                    });
        }
    }

    @Override
    public void showPlacePicker() {
        try {
            if (getActivity() != null) {
                PlacePicker.IntentBuilder placePickerBuilder = new PlacePicker.IntentBuilder();
                Intent i = placePickerBuilder.build(getActivity());
                startActivityForResult(i, PLACE_PICKER_REQUEST);
            }
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void startForecastFragment(int cityId) {
        addFragmentInParentActivity(ForecastFragment.newInstance(cityId));
    }

    private void setupToolbar() {
        setActionBar(mToolbar);
        mToolbarTitle.setText(R.string.app_name);
    }

    private void addFragmentInParentActivity(Fragment fragment) {
        if (getActivity() != null && getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).addFragment(fragment);
        }
    }

}
