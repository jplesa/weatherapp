package com.android.weatherapp.currentweather;

import com.android.weatherapp.common.base.BaseContractPresenter;
import com.android.weatherapp.common.base.BaseContractView;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
@SuppressWarnings("unused")
public interface CurrentWeatherContract {

    interface View extends BaseContractView {

        void requestLocationPermission(int requestCode);

        boolean isLocationPermissionGranted();

        void requestLastLocation();

        void showPlacePicker();

        void startForecastFragment(int cityId);

        void setCity(String city);

        void setTemperature(Float temperature);

        void setPressure(Float pressure);

        void setHumidity(Float humidity);

        void setMinTemperature(Float minTemperature);

        void setMaxTemperature(Float maxTemperature);

        void showLoadingIndicator();

        void hideLoadingIndicator();

        void showServerError();

        void showMaximumRequestsError();

    }

    interface Presenter extends BaseContractPresenter {

        void onLocationPermissionGranted(int requestCode);

        void onSearchCityClicked();

        void getCurrentWeather(double latitude, double longitude);

        void onRefreshButtonClicked();

        void onForecastButtonClicked();

    }
}
