package com.android.weatherapp.currentweather.presenters;

import com.android.weatherapp.common.C;
import com.android.weatherapp.currentweather.CurrentWeatherContract;
import com.android.weatherapp.data.OpenWeatherApi;
import com.android.weatherapp.data.models.CurrentWeather;
import com.android.weatherapp.data.models.MainWeatherInfo;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import retrofit2.HttpException;

/**
 * Created by Jurica Plesa on 23.9.2018..
 */
public class CurrentWeatherPresenter implements CurrentWeatherContract.Presenter {

    public static final int REQUEST_PLACE_PICK_PERMISSION = 10;
    public static final int REQUEST_LAST_LOCATION_PERMISSIONS = 20;

    private CurrentWeatherContract.View mView;
    private OpenWeatherApi mOpenWeatherApi;
    private Scheduler mProcessScheduler;
    private Scheduler mAndroidScheduler;
    private CompositeDisposable mDisposables = new CompositeDisposable();

    private Integer mLastReceivedCityId;

    public CurrentWeatherPresenter(CurrentWeatherContract.View view, OpenWeatherApi openWeatherApi, Scheduler processScheduler, Scheduler androidScheduler) {
        this.mView = view;
        this.mOpenWeatherApi = openWeatherApi;
        this.mProcessScheduler = processScheduler;
        this.mAndroidScheduler = androidScheduler;
    }

    @Override
    public void onLocationPermissionGranted(int requestCode) {
        switch (requestCode) {
            case REQUEST_PLACE_PICK_PERMISSION:
                if (mView.isActive()) {
                    mView.showPlacePicker();
                }
                break;
            case REQUEST_LAST_LOCATION_PERMISSIONS:
                if (mView.isActive()) {
                    mView.requestLastLocation();
                }
                break;
        }
    }

    @Override
    public void onSearchCityClicked() {
        if (!mView.isActive()) {
            return;
        }

        if (mView.isLocationPermissionGranted()) {
            mView.showPlacePicker();
        } else {
            mView.requestLocationPermission(REQUEST_PLACE_PICK_PERMISSION);
        }
    }

    @Override
    public void getCurrentWeather(double latitude, double longitude) {
        if (!mView.isActive()) {
            return;
        }

        DisposableObserver<CurrentWeather> currentWeatherObserver = getCurrentWeatherObserver();
        mDisposables.add(currentWeatherObserver);

        mView.showLoadingIndicator();

        mOpenWeatherApi.getCurrentWeather(latitude, longitude)
                .subscribeOn(mProcessScheduler)
                .observeOn(mAndroidScheduler, true)
                .subscribe(currentWeatherObserver);
    }

    @Override
    public void onRefreshButtonClicked() {
        if (mLastReceivedCityId != null) {
            getCurrentWeather(mLastReceivedCityId);
        }
    }

    @Override
    public void onForecastButtonClicked() {
        if (!mView.isActive()) {
            return;
        }

        if (mLastReceivedCityId != null) {
            mView.startForecastFragment(mLastReceivedCityId);
        }
    }

    @Override
    public void subscribe() {
        if (!mView.isActive()) {
            return;
        }

        if(mLastReceivedCityId != null) {
            getCurrentWeather(mLastReceivedCityId);
        } else {
            if (mView.isLocationPermissionGranted()) {
                mView.requestLastLocation();
            } else {
                mView.requestLocationPermission(REQUEST_LAST_LOCATION_PERMISSIONS);
            }
        }
    }

    @Override
    public void unsubscribe() {
        mDisposables.clear();
    }

    @SuppressWarnings("WeakerAccess")
    public void getCurrentWeather(int cityId) {
        if (!mView.isActive()) {
            return;
        }

        DisposableObserver<CurrentWeather> currentWeatherObserver = getCurrentWeatherObserver();
        mDisposables.add(currentWeatherObserver);

        mView.showLoadingIndicator();

        mOpenWeatherApi.getCurrentWeather(cityId)
                .subscribeOn(mProcessScheduler)
                .observeOn(mAndroidScheduler, true)
                .subscribe(currentWeatherObserver);
    }

    private DisposableObserver<CurrentWeather> getCurrentWeatherObserver() {
        return new DisposableObserver<CurrentWeather>() {
            @Override
            public void onNext(CurrentWeather currentWeather) {
                if (!mView.isActive()) {
                    return;
                }

                if (currentWeather != null) {
                    mLastReceivedCityId = currentWeather.getId();
                    mView.setCity(currentWeather.getCity());
                    MainWeatherInfo mainWeatherInfo = currentWeather.getMainWeatherInfo();
                    //noinspection ConstantConditions
                    if (mainWeatherInfo != null) {
                        mView.setTemperature(mainWeatherInfo.getTemperature());
                        mView.setHumidity(mainWeatherInfo.getHumidity());
                        mView.setPressure(mainWeatherInfo.getPressure());
                        mView.setMinTemperature(mainWeatherInfo.getMinTemperature());
                        mView.setMaxTemperature(mainWeatherInfo.getMaxTemperature());
                    }
                }
                mView.hideLoadingIndicator();
            }

            @Override
            public void onError(Throwable e) {
                if (!mView.isActive()) {
                    return;
                }
                if (e instanceof HttpException) {
                    int code = ((HttpException) e).code();
                    switch (code) {
                        case C.HTTP_TOO_MANY_REQUESTS:
                            mView.showMaximumRequestsError();
                            break;
                        default:
                            mView.showServerError();
                            break;
                    }
                } else {
                    mView.showServerError();
                }
                mView.hideLoadingIndicator();
            }

            @Override
            public void onComplete() {

            }
        };
    }

}
